JSON:API Entity Translation Support
-----------------------------------

Provides entity translation support using a decoupled module for JSON:API.

Inspired by different patches given in the following issues:

https://www.drupal.org/project/jsonapi/issues/2794431
https://www.drupal.org/project/drupal/issues/2135829
https://www.drupal.org/project/jsonapi_extras/issues/3045087

Whilst this module may not serve the wider issues surrounding entity translation support it fills a major gap for projects wanting to be able to push translations using a single structured payload.

Be sure to include the language prefix in the request URL in order to set the entities base language, follow payload.json as an example.