<?php

namespace Drupal\jsonapi\Normalizer\ImpostorFrom\jsonapi_ets;

use Drupal\jsonapi_ets\Normalizer\ContentEntityDenormalizer;

/**
 * Impostor normalizer for ContentEntityDenormalizer.
 */
class ContentEntityDenormalizerImpostor extends ContentEntityDenormalizer {}
