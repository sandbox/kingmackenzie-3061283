<?php

namespace Drupal\jsonapi\Normalizer\ImpostorFrom\jsonapi_ets;

use Drupal\jsonapi_ets\Normalizer\FieldItemNormalizer;

/**
 * Impostor normalizer for FieldItemNormalizer.
 */
class FieldItemNormalizerImpostor extends FieldItemNormalizer {}
