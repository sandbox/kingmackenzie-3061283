<?php

namespace Drupal\jsonapi_ets\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class JsonapiEtsExtensionPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    // Enable normalizers in the "src-impostor-normalizers" directory to be
    // within the \Drupal\jsonapi\Normalizer namespace in order to circumvent
    // the encapsulation enforced by
    // \Drupal\jsonapi\Serializer\Serializer::__construct().
    $container_namespaces = $container->getParameter('container.namespaces');
    $container_modules = $container->getParameter('container.modules');
    $impostor_dir = dirname($container_modules['jsonapi_ets']['pathname']) . '/src-impostor-normalizers';
    $container_namespaces['Drupal\jsonapi\Normalizer\ImpostorFrom\jsonapi_ets'][] = $impostor_dir;
    $container->setParameter('container.namespaces', $container_namespaces);
    // Since the impostor classes get initialised during compilation but don't physically
    // exist, we must require them during the service container creation.
    $container->getDefinition('serializer.normalizer.field_item.jsonapi_ets')->setFile($impostor_dir . '/FieldItemNormalizerImpostor.php');
    $container->getDefinition('serializer.normalizer.content_entity.jsonapi_ets')->setFile($impostor_dir . '/ContentEntityDenormalizerImpostor.php');
  }
}
