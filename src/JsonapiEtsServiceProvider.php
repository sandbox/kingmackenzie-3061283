<?php

namespace Drupal\jsonapi_ets;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\jsonapi_ets\Compiler\JsonapiEtsExtensionPass;

/**
 * Replace the resource type repository for our own configurable version.
 */
class JsonapiEtsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new JsonapiEtsExtensionPass());
  }
}
