<?php

namespace Drupal\jsonapi_ets\Normalizer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\jsonapi\ResourceType\ResourceType;
use Drupal\jsonapi\Normalizer\ContentEntityDenormalizer as JsonapiContentEntityDenormalizer;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * {@inheritdoc}
 */
class ContentEntityDenormalizer extends JsonApiNormalizerDecoratorBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The field plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs an EntityDenormalizerBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $plugin_manager
   *   The plugin manager for fields.
   */
  public function __construct(JsonapiContentEntityDenormalizer $inner, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $field_manager, FieldTypePluginManagerInterface $plugin_manager) {
    parent::__construct($inner);
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
    $this->pluginManager = $plugin_manager;
  }

    /**
     * {@inheritdoc}
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    if (empty($context['resource_type']) || !$context['resource_type'] instanceof ResourceType) {
      throw new PreconditionFailedHttpException('Missing context during denormalization.');
    }
    /* @var \Drupal\jsonapi\ResourceType\ResourceType $resource_type */
    $resource_type = $context['resource_type'];
    $entity_type_id = $resource_type->getEntityTypeId();
    $entity_type = $this->entityTypeManager->getDefinition( $entity_type_id);
    $bundle = $resource_type->getBundle();
    $bundle_key = $entity_type->getKey('bundle');
    $default_langcode_key = $entity_type->getKey('default_langcode');
    $langcode_key = $entity_type->getKey('langcode');
    $values = [];

    // Figure out the language to use.
    if (isset($data[$default_langcode_key])) {
      // Find the field item for which the default_langcode value is set to 1 and
      // set the langcode the right default language.
      foreach ($data[$default_langcode_key] as $item) {
        if (!empty($item['value']) && isset($item['langcode'])) {
          $values[$langcode_key] = $item['langcode'];
          break;
        }
      }
      // Remove the default langcode so it does not get iterated over below.
      unset($data[$default_langcode_key]);
    }

    if ($entity_type->hasKey('bundle')) {
      $values[$bundle_key] = $bundle;
      // Unset the bundle key from data, if it's there.
      unset($data[$bundle_key]);
    }

    $context['entity'] = $this->entityTypeManager->getStorage($entity_type_id)->create($values);

    $this->prepareInput($data, $resource_type, $format, $context);

    return $context['entity'];
  }

  /**
   * Prepares the input data to create the entity.
   *
   * @param array $data
   *   The input data to modify.
   * @param \Drupal\jsonapi\ResourceType\ResourceType $resource_type
   *   Contains the info about the resource type.
   * @param string $format
   *   Format the given data was extracted from.
   * @param array $context
   *   Options available to the denormalizer.
   *
   * @return array
   *   The modified input data.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function prepareInput(array $data, ResourceType $resource_type, $format, array $context) {
    $data_internal = [];

    $field_map = $this->fieldManager->getFieldMap()[$resource_type->getEntityTypeId()];

    $entity_type_id = $resource_type->getEntityTypeId();
    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundle_key = $entity_type_definition->getKey('bundle');
    $uuid_key = $entity_type_definition->getKey('uuid');

    // Translate the public fields into the entity fields.
    foreach ($data as $public_field_name => $field_value) {
      $internal_name = $resource_type->getInternalName($public_field_name);

      // Skip any disabled field, except the always required bundle key and
      // required-in-case-of-PATCHing uuid key.
      // @see \Drupal\jsonapi\ResourceType\ResourceTypeRepository::getFieldMapping()
      if ($resource_type->hasField($internal_name) && !$resource_type->isFieldEnabled($internal_name) && $bundle_key !== $internal_name && $uuid_key !== $internal_name) {
        continue;
      }

      if (!isset($field_map[$internal_name]) || !in_array($resource_type->getBundle(), $field_map[$internal_name]['bundles'], TRUE)) {
        throw new UnprocessableEntityHttpException(sprintf(
          'The attribute %s does not exist on the %s resource type.',
          $internal_name,
          $resource_type->getTypeName()
        ));
      }

      $field_type = $field_map[$internal_name]['type'];
      $field_class = $this->pluginManager->getDefinition($field_type)['list_class'];
      $field_item_list = $context['entity']->get($internal_name);
      // We must reset the field values to avoid getting multiple items for a single language.
      $field_item_list->setValue([]);

      if ($field_value) {
        // The field instance must be passed in the context so that the field
        // denormalizer can update field values for the parent entity.
        $context['target_instance'] = $field_item_list->appendItem();
        $field_denormalization_context = array_merge($context, [
          'field_type' => $field_type,
          'field_name' => $internal_name,
          'field_definition' => $this->fieldManager->getFieldDefinitions($resource_type->getEntityTypeId(), $resource_type->getBundle())[$internal_name],
        ]);
        $data_internal[$internal_name] = \Drupal::service('jsonapi.serializer')->denormalize($field_value, $field_class, $format, $field_denormalization_context);
      }
    }

    return $data_internal;
  }

}
