<?php

namespace Drupal\jsonapi_ets\Normalizer;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\TypedData\FieldItemDataDefinitionInterface;
use Drupal\jsonapi\Normalizer\FieldItemNormalizer as JsonapiFieldItemNormalizer;
use Drupal\serialization\Normalizer\SerializedColumnNormalizerTrait;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;

/**
 * {@inheritdoc}
 */
class FieldItemNormalizer extends JsonApiNormalizerDecoratorBase {

  use SerializedColumnNormalizerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(JsonapiFieldItemNormalizer $inner, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($inner);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    $item_definition = $context['field_definition']->getItemDefinition();
    assert($item_definition instanceof FieldItemDataDefinitionInterface);

    if (!isset($context['target_instance'])) {
      throw new InvalidArgumentException('$context[\'target_instance\'] must be set to denormalize with the FieldItemNormalizer');
    }
    if ($context['target_instance']->getParent() == NULL) {
      throw new InvalidArgumentException('The field item passed in via $context[\'target_instance\'] must have a parent set.');
    }

    $field_item = $context['target_instance'];
    $this->checkForSerializedStrings($data, $class, $field_item);

    $field_item = $this->getTranslatedFieldItemInstance($data, $field_item);

    $property_definitions = $item_definition->getPropertyDefinitions();

    $serialized_property_names = $this->getCustomSerializedPropertyNames($field_item);
    $denormalize_property = function ($property_name, $property_value, $property_value_class, $format, $context) use ($serialized_property_names) {
      if ($this->inner->supportsDenormalization($property_value, $property_value_class, $format, $context)) {
        return $this->inner->denormalize($property_value, $property_value_class, $format, $context);
      }
      else {
        if (in_array($property_name, $serialized_property_names, TRUE)) {
          $property_value = serialize($property_value);
        }
        return $property_value;
      }
    };
    // Because e.g. the 'bundle' entity key field requires field values to not
    // be expanded to an array of all properties, we special-case single-value
    // properties.
    if (!is_array($data)) {
      $property_value = $data;
      $property_name = $item_definition->getMainPropertyName();
      $property_value_class = $property_definitions[$property_name]->getClass();
      return $denormalize_property($property_name, $property_value, $property_value_class, $format, $context);
    }

    $data_internal = [];
    if (!empty($property_definitions)) {
      foreach ($data as $property_name => $property_value) {
        $property_value_class = $property_definitions[$property_name]->getClass();
        $data_internal[$property_name] = $denormalize_property($property_name, $property_value, $property_value_class, $format, $context);
      }
    }
    else {
      $data_internal = $data;
    }

    $field_item->setValue($this->constructValue($data_internal, $context));

    return $field_item;
  }

  /**
   * Build the field item value using the incoming data.
   *
   * @param $data
   *   The incoming data for this field item.
   * @param $context
   *   The context passed into the Normalizer.
   *
   * @return mixed
   *   The value to use in Entity::setValue().
   */
  protected function constructValue($data, $context) {
    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    $field_item = $context['target_instance'];
    $serialized_property_names = $this->getCustomSerializedPropertyNames($field_item);

    // Explicitly serialize the input, unlike properties that rely on
    // being automatically serialized, manually managed serialized properties
    // expect to receive serialized input.
    foreach ($serialized_property_names as $serialized_property_name) {
      if (is_array($data) && array_key_exists($serialized_property_name, $data)) {
        $data[$serialized_property_name] = serialize($data[$serialized_property_name]);
      }
    }

    return $data;
  }

    /**
     * Gets a field item instance for use with SerializedColumnNormalizerTrait.
     *
     * @param $data
     *   Json decoded data.
     * @param \Drupal\Core\Field\FieldItemInterface $item
     *   The field item definition of the instance to get.
     * @return \Drupal\Core\TypedData\TypedDataInterface
     *
     * @throws \Drupal\Core\TypedData\Exception\MissingDataException
     * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
     */
  protected function getTranslatedFieldItemInstance(&$data, FieldItemInterface $item) {
    $entity = $item->getEntity();
    $item_field_definition = $item->getFieldDefinition();
    $field_name = $item_field_definition->getName();

    $set_field_item = function ($item, $field) {
      $cardinality = $item->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
      if ($cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED || count($field) < $cardinality) {
        // Since we are forcing an empty value in order to service translatable field structures,
        // for multi-valued fields it will throw a validation error since it will append to the empty item
        // rather than replace it. In this instance we are forced to remove it.
        $field_value = $field->getValue();
        if (is_array($field_value) && isset($field_value[0]) && empty($field_value[0])) {
          $field->removeItem(key($field_value));
        }
        $field_item = $field->appendItem();
      }
      else {
        $field_item = $field->first();
      }
      assert($field_item instanceof FieldItemInterface);
      return $field_item;
    };

    // If this field is translatable, we need to create a translated instance.
    if (isset($data['langcode'])) {
      $langcode = $data['langcode'];
      unset($data['langcode']);
      if ($item_field_definition->isTranslatable()) {
        $entity_translation = $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : $this->createTranslatedEntity($entity, $langcode, $field_name);
        $field = $entity_translation->get($field_name);
        assert($field instanceof FieldItemListInterface);
        $field_item = $set_field_item($item, $field);
      }
    }
    else {
      $field = $entity->get($field_name);
      assert($field instanceof FieldItemListInterface);
      $field_item = $set_field_item($item, $field);
    }

    return $field_item;
  }

  /**
   * Create an empty entity translation to fill with field data.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *    The untranslated entity.
   * @param string $langcode
   *    The langcode.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *    The translated entity.
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  protected function createTranslatedEntity(Entity $entity, $langcode, $field_name) {
    // Create a new translation. /** @var \Drupal\Core\TypedData\TranslatableInterface $entity */
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity_translation */
    $entity_translation = $entity->addTranslation($langcode);

    // Remove all default values, except for the langcode.
    $translated_fields = $entity_translation->getTranslatableFields(FALSE);

    if (isset($translated_fields[$field_name])) {
      $field = $translated_fields[$field_name];
      $field->setValue([]);
    }

    return $entity_translation;
  }

}
