<?php

namespace Drupal\jsonapi_ets\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\jsonapi\Access\EntityAccessChecker;
use Drupal\jsonapi\Context\FieldResolver;
use Drupal\jsonapi\Controller\EntityResource as JsonapiEntityResource;
use Drupal\jsonapi\IncludeResolver;
use Drupal\jsonapi\ResourceType\ResourceType;
use Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * {@inheritDoc}
 */
class EntityResource extends JsonapiEntityResource {

  /**
   * The JSON:API inner service.
   *
   * @var JsonapiEntityResource
   */
  protected $inner;

  /**
   * {@inheritDoc}
   */
  public function __construct(JsonapiEntityResource $inner, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $field_manager, ResourceTypeRepositoryInterface $resource_type_repository, RendererInterface $renderer, EntityRepositoryInterface $entity_repository, IncludeResolver $include_resolver, EntityAccessChecker $entity_access_checker, FieldResolver $field_resolver, SerializerInterface $serializer, TimeInterface $time, AccountInterface $user) {
    $this->inner = $inner;
    parent::__construct($entity_type_manager, $field_manager, $resource_type_repository, $renderer, $entity_repository, $include_resolver, $entity_access_checker, $field_resolver, $serializer, $time, $user);
  }

  /**
   * {@inheritDoc}
   */
  protected function updateEntityField(ResourceType $resource_type, EntityInterface $origin, EntityInterface $destination, $field_name) {
    // The update is different for configuration entities and content entities.
    if ($origin instanceof ContentEntityInterface && $destination instanceof ContentEntityInterface) {
      // First scenario: both are content entities.
      $field_name = $resource_type->getInternalName($field_name);
      if ($destination->isTranslatable()) {
        foreach ($origin->getTranslationLanguages() as $langcode => $language) {
          if (!$destination->hasTranslation($langcode)) {
            $destination->addTranslation($langcode, $origin->toArray());
          }
          $field = $origin->getTranslation($langcode)->get($field_name);
          // It is not possible to set the language to NULL as it is automatically
          // re-initialized. As it must not be empty, skip it if it is.
          // @todo Remove in https://www.drupal.org/project/drupal/issues/2933408.
          if ($origin->getTranslation($langcode)->getEntityType()->hasKey('langcode') && $field_name === $origin->getTranslation($langcode)->getEntityType()->getKey('langcode') && $field->isEmpty()) {
            continue;
          }
          if ($this->inner->checkPatchFieldAccess($destination->getTranslation($langcode)->get($field_name), $field)) {
            $destination->getTranslation($langcode)->set($field_name, $field->getValue());
          }
        }
      }
      else {
        $destination_field_list = $destination->get($field_name);
        $origin_field_list = $origin->get($field_name);
        if ($this->inner->checkPatchFieldAccess($destination_field_list, $origin_field_list)) {
          $destination->set($field_name, $origin_field_list->getValue());
        }
      }
    }
    elseif ($origin instanceof ConfigEntityInterface && $destination instanceof ConfigEntityInterface) {
      // Second scenario: both are config entities.
      $destination->set($field_name, $origin->get($field_name));
    }
    else {
      throw new BadRequestHttpException('The serialized entity and the destination entity are of different types.');
    }
  }

  /**
   * Magic method to return any method call inside the inner service.
   */
  public function __call($method, $args) {
    return call_user_func_array(array(
      $this->inner,
      $method
    ), $args);
  }

}
